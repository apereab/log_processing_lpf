# Challenge python
---
---
## Descripción de la situación inicial desde la que se parte

Es un supuesto desde el cual arrancar, pudiendo ser diferente la solución si esto cambia o incluso sin cambiar puede haber solcuiones más optimas**

Suponemos que los ficheros se dejan en un unico repositorio y que el script lo vamos a hacer correr en el mismo servidor donde se aloja ese repositorio.

En la carpeta logfiles se depositan ficheros logs, -pueden venir de diferentes origenes-, sin una frecuencia indicada.

La estructura de dichos ficheros es muy sencilla:
- 1610950440 gandalf tooler
- 1610950481 higgins electron
- 1610950522 sbmaster01 sbrma01
Sin cabecera

Siendo el primer campo un valor timestamp, después el nombre de un servidor que inicia una conexión y el último el servidor destino de la conexión.

---
---
## Objetivo
Existen varios objetivos que vamos a comentar ahora mismo

1. Procesar todos los archivos logs y dejar toda la información en un repositorio común de más sencillo procesamiento. 

El proceso se lanzará de manera programada cada X timepo -estimamos cada hora por ejemplo-.
Verifica si hay ficheros -pueden ser uno más ficheros-, en la carpeta origen de los logs. Si hay alguno, los lee uno a uno y línea a línea. Aquí se persigue un poco mostrar que es muy posible que cada línea haya que realizar algún pequeño proceso. En este caso solo pasamos la fecha de timestamp a otro formato fecha más "humana".
Luego, con cada línea leída se guarda en un fichero diario. Si el fichero no existe, -porque puede ser la primera ejecución del día con datos-, se crea. 
- El nombre del fichero sigue el siguiente formato: results + YYYYMMDD + .csv
- La estructura es similar a los origines, pero añadiendole cabecera y la columna de la fecha en un formato más humano

Resumen: Al final deberá existir un fichero .csv de resultados por cada día que haya habido logs que procesar.

2. Analizar los ficheros logs después de cada ejecución

Cada vez que el proceso se ejecute, debe realizar un análisis de todos los ficheros de resultados resolviendo los tres siguientes puntos y mostrandolos en pantalla. La verdad que lo logico sería que lo dejase en algún sistema que posibilite su explotación incluso en tiempo real, pero no ha habido tiempo. 
    
    NOTA: La idea hubiese sido dejar los resultados de este procesado en una BD's tipo mongoDB y con un pequeño front -Angular y node.js-, mostrar un dashboard de resultados. Algo sencillo. Aquí, simplemente por cumplir un poco con los requisitos se muestra por pantalla

- Lista de host conectados a un host dado en la configuración. Variable donde indicamos el nombre del host: HOST_SOURCE
    
    Funcion: hostnames_connected
- Lista de host que se han conectado a un host dado en la configuración. Variable donde indicamos el nombre del host: HOST_TARGET

    Funcion: hostnames_received
- Host que haya realizado y recibido -la suma de ambas- más conexiones

    Funcion: hostname_that_generated_most_connections
---
---

### Instalación y ejecución

Ejecutar para instalar todo lo necesario 

```sh
$ pip install -r requirements.txt
```

Después, lanzamos la aplicación

```sh
$ python app.py
```

### Pruebas

Como he comentado, el sistema está pensado para que se ejecute cada hora, ingeste los nuevos registros y realice un análisis de los datos existentes en un rango de tiempo configurado en el propio script. Este rango viene indicado por la hora actual y el número de horas a restar. Este número se indica en la variable: HOURS_NUMBER=1 que es su valor por defecto. 
Con la configuración por defecto, para por ejemplo una ejecución a las 7:10 del día 22 enero del 2021, nos hará el análisis de todos los datos recuperados del repositorio de resultados cuyo campo "date" esté dentro del rango siguiente:
- Fecha inicio: Fecha y hora actual - HOURS_NUMBER -> 22 enero del 2021 6:10 AM
- Fecha fin: Fecha y hora actuales -> 22 enero del 2021 7:10 AM

Logicamente, si lo ejecutamos tal cual el proceso no devolverá ningún resultado si no le ponemos datos a procesar en el rango de hora en el que se esté ejecutando, pero podemos simular o conseguir que nos devuelva datos de dos modos.
    
    NOTA: Los datos de muestra están en el rango de 18 de enero del 2021 6:50 AM hasta el 21 de enero del 2021 8:00 AM más o menos

- Caso 1: Cambiando el valor de horas a restar. Por ejemplo, 100 nos retrocederá unos 4 días. Este valor hay que ajsutarlo al día que se esté ejecutando la prueba
- Caso 2: En la función que lee los ficheros de resultados y añade el primer filtro de fecha podemos aplicar el rango de manera manual y no automática 

```sh
106 ####
107 # for simulate other date
108 #date_time_str = '2021-01-18 06:10:26.243860'
109 #date_now = datetime.strptime(date_time_str, '%Y-%m-%d %H:%M:%S.%f')
110 ####
```
Solo tendríamos que cambiar el valor date_time_str por una fecha en el rango que he indicado y descomentar ambas líneas, la 108 y 109 y proceder a la ejecución.

---
---

## Aclaraciones

Aquí indico ciertas aclaraciones sobre decisiones tomadas que pueden resutlar de interés.

1. He considerado que una solución de scripting puede ser la solución más rápida y que el volumen de datos a procesar es soportado por el servidor donde se va a ejecutar, siendo el tiempo de ejecución siempre inferior a la ventana de 1 hora que se estima que es el tiempo que pasa entre cada vez que se requieren los resultados procesados

Pensamos que si lo que buscamos es una solución a prueba de balas deberíamos estudiar una situación y uan solución muy diferente. Por ejemplo, nos iriamos a una solución cloud con una arquitectura más o menos como la siguiente:
 - Repositorios de los ficheros en Azure Data Lake Storage Gen2
 - El proceso lo gestionariamos con Azure Data Factory
 - Separariamos el proceso en dos. Recuperación de los datos y procesado
 - Recuperación de los datos por ejemplo con una Azure Function la cual se ejecutaría solo cuando llegasen nuevos ficheros al logfiles -ficheros a procesar-. Como sabemos, las functions solo tienen coste por uso
 - Procesado de los datos programado cada 1. Igualmente, una Azure Function, si el volumen de datos es tolerable o en su defecto y ya a prueba de fuego nos iriamos a un cuaderno de databricks realizado en pyspark para ejecutar el análisis en un cluster de spark

2. He usado ademas de pandas, DASK con el objetivo de obtener la facilidad de uso de pandas, pero añadiendo el multriproceso. Como sabemos, pandas utiliza un unico nucleo para el procesado, por lo que en sistemas de un tamaño considerable, lo ideal es utilizar al máximo toda la CPU. Esto es la teoría que va muy bien, pero que requiere de ciertos ajustes de cara al contexto donde vayamos a ejecutarlo. Si no está bien y el volumen de datos no es grande, puede que el beneficio no sea importante o peor, que perjudique el resultado final. Aquí, trantandose de un PoC/Challenge he preferido ponerlo. Indudablemente, se requiere su afinamiento para mostrar un calro aumento del rendimiento.

3. Una mejora que habría que añadir pensando que esto pudiese ponerse así en explotación sería un log de la propia aplicación indicando como mínimo la fecha y hora de la ejecución y tal vez possibles errores