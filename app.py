#!/usr/bin/env python3
import sys
import os
import re
import datetime
import pandas as pd
import numpy as np
import dask.dataframe as dd
import multiprocessing
from datetime import date, datetime, time, timedelta
import shutil

# folder name of files logs pending processing
PENDING_LOG_FILES_PATH = "logfiles" + '/'
# folder name of files logs processed
PROCESSED_LOG_FILES_PATH = "processedlogfiles" + '/'
# folder name of results files
RESULT_LOG_FILES_PATH = "resultlogfiles" + '/'
# root path of the results file name
FILE_NAME_LOGS_RESULTS = "results"
# file extension of results files
RESULT_FILE_EXTENSION = ".csv"
# new header of result file
HEADER_OF_NEW_FILE = "date;host_source;host_target\n"
# number of hours to analyse
HOURS_NUMBER = 1

# (1) Hostname of which you want to know the list of hosts connected in the last hour
HOST_SOURCE = "higgins"
# (2) Hostname to which we want to audit its connections
HOST_TARGET = "tooler"

def log_process(log_file):
    # read & aggregate logs to main log
    returned_logs = []
    with open(PENDING_LOG_FILES_PATH + log_file, mode='r',encoding='UTF-8') as file:
        for log in  file.readlines():
            #print(log)
            valores = log.split()
            valores[0] = datetime.fromtimestamp(int(valores[0])).strftime('%c')
            log = ';'.join(valores) + '\n'
            returned_logs.append(log)
        file.close()

    # move file at processed folder of processed files
    shutil.move(PENDING_LOG_FILES_PATH + log_file, PROCESSED_LOG_FILES_PATH + log_file)
    return returned_logs    
  
def file_output(returned_logs):
    # add new logs at day log file
    # if not exist day file log, is created
    today = datetime.today()
    day = today.strftime("%Y%m%d")
    path = RESULT_LOG_FILES_PATH + FILE_NAME_LOGS_RESULTS + day + RESULT_FILE_EXTENSION
    try:
        with open(path, 'x') as file:
            file.write(HEADER_OF_NEW_FILE)
            for log in returned_logs:
                file.write(log)
            file.close()
    except FileExistsError:
        with open(path, 'a') as file: 
            for log in returned_logs:
                file.write(log)
            file.close()

def hostnames_connected(df):
    # a list of hostnames connected to a given (configurable) host during the last hour
    # filter
    df = df[df.host_target == HOST_TARGET]
    # groupby
    value = df.groupby('host_source').host_target.count().compute().reset_index(name="total")
    #value = df.groupby('host_source').host_target.count().compute()
    print('*************')
    print('-- point 1 --')
    print('-------- filter (HOST_TARGET): ', HOST_TARGET, '\n', value)

def hostnames_received(df):
    # a list of hostnames received connections from a given (configurable) host during the last hour
    # filter
    df = df[df.host_source == HOST_SOURCE]
    # groupby
    value = df.groupby('host_target').host_source.count().compute().reset_index(name="total")
    print('*************')
    print('-- point 2 --')    
    print('-------- filter (HOST_SOURCE): ', HOST_SOURCE, '\n', value)   

def hostname_that_generated_most_connections(df):
    # hostname that generated most connections in the last hour
    df_target = df.groupby('host_target').host_source.count().compute().reset_index(name="total")
    df_source = df.groupby('host_source').host_source.count().compute().reset_index(name="total")
    df_target.columns = ['host', 'total']
    df_source.columns = ['host', 'total']
    print('*************')
    print('-- point 3 --')
    print('-- hostname that generated most connections in the last hour --')
    print(pd.concat([df_target, df_source]).sort_values(by=['total'], ascending=False).head(1))
    print('-- end --')

def read_date():
    # read all files and load at Dask Dataframe
    df = dd.read_csv(RESULT_LOG_FILES_PATH + FILE_NAME_LOGS_RESULTS + '*' + RESULT_FILE_EXTENSION, sep=';', parse_dates=['date'])
    print('-------- num elements: ', df.shape[0].compute())
    # filter by date
    date_now = datetime.now()
    ####
    # for simulate other date
    #date_time_str = '2021-01-18 06:10:26.243860'
    #date_now = datetime.strptime(date_time_str, '%Y-%m-%d %H:%M:%S.%f')
    ####
    end_date = pd.Timestamp(date_now)
    start_date = pd.Timestamp(date_now - timedelta(hours=HOURS_NUMBER))
    print('start_date: ', start_date, ' - end_date: ', end_date)
    df = df[(df['date'] > start_date) & (df['date'] <= end_date)]
    print('-------- num elements: ', df.shape[0].compute())
    return df    

def process_results():
    df = read_date()
    hostnames_connected(df)
    hostnames_received(df)
    hostname_that_generated_most_connections(df)

if __name__ == "__main__":   
    # we process the log files of the folder
    arr = os.listdir(PENDING_LOG_FILES_PATH)
    returned_logs = []
    for log_file in arr:
        returned_logs = returned_logs + log_process(log_file)
  
    # add new logs at day log file
    if returned_logs != []:
      file_output(returned_logs)
    
    # challenge objetive
    start_time = datetime.now()
    process_results()
    end_time = datetime.now()
    print('Duration results process: {}'.format(end_time - start_time))

    sys.exit(0)